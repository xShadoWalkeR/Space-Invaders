var class_space_invaders_1_1_double_buffer2_d =
[
    [ "DoubleBuffer2D", "class_space_invaders_1_1_double_buffer2_d.html#a0a628d8e0e7d7a9cd408639cb8b6deeb", null ],
    [ "Clear", "class_space_invaders_1_1_double_buffer2_d.html#ac12bc1056b631dc9006569d4bc2c8d1d", null ],
    [ "Display", "class_space_invaders_1_1_double_buffer2_d.html#abb64be324b1c3e935bdd377174967448", null ],
    [ "Swap", "class_space_invaders_1_1_double_buffer2_d.html#a32d8e4c959aefff6414a4f5f3a16352d", null ],
    [ "current", "class_space_invaders_1_1_double_buffer2_d.html#ab379b8c6b3596c13f183578cbae2fa2c", null ],
    [ "XDim", "class_space_invaders_1_1_double_buffer2_d.html#a96873ffa7d903bc757616d437a0c6e0f", null ],
    [ "YDim", "class_space_invaders_1_1_double_buffer2_d.html#aacb061018a00300a51b85de3edb41f25", null ],
    [ "this[int x, int y]", "class_space_invaders_1_1_double_buffer2_d.html#ab10e5874744301fbfebe483bf8e13f05", null ]
];