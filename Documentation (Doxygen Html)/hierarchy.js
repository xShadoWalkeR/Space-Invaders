var hierarchy =
[
    [ "SpaceInvaders.Barrier", "class_space_invaders_1_1_barrier.html", null ],
    [ "SpaceInvaders.BetterConsole", "class_space_invaders_1_1_better_console.html", null ],
    [ "SpaceInvaders.Bullet", "class_space_invaders_1_1_bullet.html", null ],
    [ "SpaceInvaders.Bullets", "class_space_invaders_1_1_bullets.html", null ],
    [ "SpaceInvaders.DoubleBuffer2D", "class_space_invaders_1_1_double_buffer2_d.html", null ],
    [ "SpaceInvaders.Explosion", "class_space_invaders_1_1_explosion.html", null ],
    [ "SpaceInvaders.Game", "class_space_invaders_1_1_game.html", null ],
    [ "SpaceInvaders.IGameObject", "interface_space_invaders_1_1_i_game_object.html", [
      [ "SpaceInvaders.GameObject", "class_space_invaders_1_1_game_object.html", [
        [ "SpaceInvaders.Barriers", "class_space_invaders_1_1_barriers.html", null ],
        [ "SpaceInvaders.Enemies", "class_space_invaders_1_1_enemies.html", null ],
        [ "SpaceInvaders.Enemy", "class_space_invaders_1_1_enemy.html", null ],
        [ "SpaceInvaders.Explosions", "class_space_invaders_1_1_explosions.html", null ],
        [ "SpaceInvaders.Ovni", "class_space_invaders_1_1_ovni.html", null ],
        [ "SpaceInvaders.Ship", "class_space_invaders_1_1_ship.html", null ]
      ] ]
    ] ],
    [ "SpaceInvaders.Menu", "class_space_invaders_1_1_menu.html", null ],
    [ "SpaceInvaders.Pixel", "struct_space_invaders_1_1_pixel.html", null ],
    [ "SpaceInvaders.Program", "class_space_invaders_1_1_program.html", null ],
    [ "SpaceInvaders.Sprites", "class_space_invaders_1_1_sprites.html", null ],
    [ "SpaceInvaders.Timer", "class_space_invaders_1_1_timer.html", null ],
    [ "SpaceInvaders.Vector2", "struct_space_invaders_1_1_vector2.html", null ]
];