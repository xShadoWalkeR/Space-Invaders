var searchData=
[
  ['igameobject_55',['IGameObject',['../interface_space_invaders_1_1_i_game_object.html',1,'SpaceInvaders']]],
  ['igameobject_2ecs_56',['IGameObject.cs',['../_i_game_object_8cs.html',1,'']]],
  ['increasey_57',['IncreaseY',['../class_space_invaders_1_1_enemy.html#a7e87c3cb3c18951380f2d1573619d2a6',1,'SpaceInvaders::Enemy']]],
  ['init_58',['Init',['../class_space_invaders_1_1_ovni.html#abc92bebc7c0656295d72dacaffbd4c14',1,'SpaceInvaders.Ovni.Init()'],['../class_space_invaders_1_1_ship.html#a94654c1d6c947ad74e76055e48c4fc6e',1,'SpaceInvaders.Ship.Init()']]],
  ['invadersstring_59',['invadersString',['../class_space_invaders_1_1_sprites.html#a9ab7642b60704e5286df88840a7c43d7',1,'SpaceInvaders::Sprites']]],
  ['iscounting_60',['IsCounting',['../class_space_invaders_1_1_timer.html#a1abb09e317b0f200916855a0f1f3f56c',1,'SpaceInvaders::Timer']]],
  ['isdestroyed_61',['IsDestroyed',['../class_space_invaders_1_1_ovni.html#a304af4cba4ee8ad188a0dac6671f042f',1,'SpaceInvaders::Ovni']]],
  ['isflying_62',['IsFlying',['../class_space_invaders_1_1_ovni.html#a60ee303c54a0c6ed28f8170d68292fa9',1,'SpaceInvaders::Ovni']]],
  ['isnotdestroyed_63',['IsNotDestroyed',['../class_space_invaders_1_1_barrier.html#a9826d80b99ea09cb447c5b89ad852271',1,'SpaceInvaders::Barrier']]]
];
