var searchData=
[
  ['setcolor_207',['SetColor',['../class_space_invaders_1_1_barrier.html#a3c5b11cd91f6c7d416449ef3dee4ebfa',1,'SpaceInvaders::Barrier']]],
  ['setdestroyed_208',['SetDestroyed',['../class_space_invaders_1_1_barrier.html#a329ef179b34be7c2d2968f526b8f101b',1,'SpaceInvaders::Barrier']]],
  ['ship_209',['Ship',['../class_space_invaders_1_1_ship.html#ad13ce871da4de6ab75d6dbdc91bd7ccf',1,'SpaceInvaders::Ship']]],
  ['start_210',['Start',['../class_space_invaders_1_1_barriers.html#ad9ef53820da68e6d934e65097d4d61d1',1,'SpaceInvaders.Barriers.Start()'],['../class_space_invaders_1_1_game_object.html#a9e0693c32f7cfccab37f4391912de276',1,'SpaceInvaders.GameObject.Start()'],['../interface_space_invaders_1_1_i_game_object.html#a0a024241cc1433490449912accf573c5',1,'SpaceInvaders.IGameObject.Start()']]],
  ['swap_211',['Swap',['../class_space_invaders_1_1_double_buffer2_d.html#a32d8e4c959aefff6414a4f5f3a16352d',1,'SpaceInvaders::DoubleBuffer2D']]]
];
