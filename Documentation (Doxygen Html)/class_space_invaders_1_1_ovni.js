var class_space_invaders_1_1_ovni =
[
    [ "Ovni", "class_space_invaders_1_1_ovni.html#aa8bb4182edacc361abf11eadeb2c6e19", null ],
    [ "Init", "class_space_invaders_1_1_ovni.html#abc92bebc7c0656295d72dacaffbd4c14", null ],
    [ "IsDestroyed", "class_space_invaders_1_1_ovni.html#a304af4cba4ee8ad188a0dac6671f042f", null ],
    [ "IsFlying", "class_space_invaders_1_1_ovni.html#a60ee303c54a0c6ed28f8170d68292fa9", null ],
    [ "Update", "class_space_invaders_1_1_ovni.html#ad00b8cd12bd4ea141fa88b70e4a0b1f1", null ],
    [ "Write", "class_space_invaders_1_1_ovni.html#ac92deeb01c3743e6c8262e2791e8f831", null ],
    [ "Coordinates", "class_space_invaders_1_1_ovni.html#a44c0b55f56ea5cc40656c10b304d5fa6", null ]
];